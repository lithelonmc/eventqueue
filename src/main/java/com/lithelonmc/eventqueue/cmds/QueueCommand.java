package com.lithelonmc.eventqueue.cmds;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Syntax;
import com.lithelonmc.eventqueue.EventQueue;
import com.lithelonmc.eventqueue.tasks.TimerTask;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

@CommandAlias("queue|q")
public class QueueCommand extends BaseCommand {
    private static final EventQueue PLUGIN = EventQueue.getInstance();

    @Subcommand("join")
    @Description("Join the current queue")
    public static void join(Player player) {
        FileConfiguration config = PLUGIN.getConfig();
        Location location = config.getLocation("spawn");
        if (location == null) {
            player.sendMessage("§4There is no event queue to join currently.");
        } else if (EventQueue.isRunning) {
            player.sendMessage("§4The event has already been started.");
        } else if (EventQueue.QUEUE_LIST.contains(player)) {
            player.sendMessage("§4You are already in the queue.");
        } else {
            EventQueue.QUEUE_LIST.add(player);
            EventQueue.sendMessage("§a" + player.getName() + " has joined the queue.");
        }
    }

    @Subcommand("leave")
    @Description("Leave the current queue")
    public static void leave(Player player) {
        if (EventQueue.QUEUE_LIST.contains(player) == false) {
            player.sendMessage("§4You are not in any event queue.");
        } else if (EventQueue.isRunning) {
            player.sendMessage("§4You cannot leave the event after it started.");
        } else {
            player.teleport(player.getWorld().getSpawnLocation());
            EventQueue.sendMessage("§a" + player.getName() + " has left the queue.");
            EventQueue.QUEUE_LIST.remove(player);
        }
    }

    @Subcommand("delete")
    @Description("Deletes the current queue and sends everyone back to spawn")
    @CommandPermission("eventqueue.admin")
    public static void delete(Player player) {
        EventQueue.isRunning = false;
        FileConfiguration config = PLUGIN.getConfig();
        config.set("spawn", null);
        PLUGIN.saveConfig();
        for (Player other : EventQueue.QUEUE_LIST) {
            other.teleport(other.getWorld().getSpawnLocation());
            other.sendMessage("§4The event has been stopped.");
        }
        EventQueue.QUEUE_LIST.clear();
        player.sendMessage("§aYou have stopped the event.");
    }

    @Subcommand("start")
    @Description("Starts the current queue after timer ends")
    @Syntax("<seconds>")
    @CommandPermission("eventqueue.admin")
    public static void start(Player player, @Default("20") int seconds) {
        FileConfiguration config = PLUGIN.getConfig();
        Location location = config.getLocation("spawn");
        if (location == null) {
            player.sendMessage("§4You did not set a queue spawn yet.");
        } else if (EventQueue.isRunning) {
            player.sendMessage("§4The event has already been started.");
        } else {
            player.sendMessage("§aThe event will start in " + seconds + " seconds.");
            new TimerTask(seconds, location).runTaskTimer(PLUGIN, 0, 20);
        }
    }

    @Subcommand("spawn")
    @CommandPermission("eventqueue.admin")
    public class SpawnCommand extends BaseCommand {
        @Default
        @Subcommand("go")
        @Description("Go to the current queue spawn")
        public void go(Player player) {
            FileConfiguration config = PLUGIN.getConfig();
            Location location = config.getLocation("spawn");
            if (location == null) return;
            player.teleport(location);
            player.sendMessage("§aYou were teleported to the queue spawn.");
        }

        @Subcommand("set")
        @Description("Sets the current queue spawn")
        public void set(Player player) {
            FileConfiguration config = PLUGIN.getConfig();
            Location location = player.getLocation();
            config.set("spawn", location);
            PLUGIN.saveConfig();
            player.sendMessage("§aThe queue spawn has been set to your current location.");
        }
    }

    @HelpCommand
    public static void help(CommandSender sender, CommandHelp help) {
        help.showHelp();
    }
}
