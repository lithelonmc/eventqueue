package com.lithelonmc.eventqueue.listeners;

import com.lithelonmc.eventqueue.EventQueue;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerListener implements Listener {
    private static final EventQueue PLUGIN = EventQueue.getInstance();

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        if (EventQueue.QUEUE_LIST.contains(player) == false) return;
        Location location = PLUGIN.getConfig().getLocation("spawn");
        if (location == null) return;
        new BukkitRunnable() {
            public void run() {
                player.teleport(location);
            }
        }.runTaskLater(PLUGIN, 1);
    }
}
