package com.lithelonmc.eventqueue.tasks;

import com.lithelonmc.eventqueue.EventQueue;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.atomic.AtomicInteger;

public class TimerTask extends BukkitRunnable {
    private final Location location;
    private final int seconds;
    private final AtomicInteger timer = new AtomicInteger(0);

    public TimerTask(int seconds, Location location) {
        this.seconds = seconds;
        this.location = location;
    }

    public void run() {
        EventQueue.isRunning = true;
        if (timer.get() == seconds) {
            for (Player player : EventQueue.QUEUE_LIST) {
                player.teleport(location);
            }
            this.cancel();
        } else {
            int remaining = seconds - timer.get();
            if (remaining % 10 == 0 || remaining < 6) {
                EventQueue.sendMessage("§aThe event is starting in " + remaining + " seconds..");
            }
            timer.incrementAndGet();
        }
    }
}
