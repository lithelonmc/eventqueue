package com.lithelonmc.eventqueue;

import co.aikar.commands.BukkitCommandManager;
import com.google.common.collect.Sets;
import com.lithelonmc.eventqueue.cmds.QueueCommand;
import com.lithelonmc.eventqueue.listeners.PlayerListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Set;

public class EventQueue extends JavaPlugin {
    @Getter
    private static EventQueue instance;
    public static final Set<Player> QUEUE_LIST = Sets.newHashSet();
    public static boolean isRunning = false;

    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new PlayerListener(), this);
        BukkitCommandManager commands = new BukkitCommandManager(this);
        commands.enableUnstableAPI("help");
        commands.registerCommand(new QueueCommand());
    }

    public static void sendMessage(String message) {
        for (Player player : QUEUE_LIST) {
            player.sendMessage(message);
        }
    }
}
